const mongoose = require('mongoose');

// Schéma de menu
const platSchema = new mongoose.Schema({

	picture: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	type:
	{
		type: String,
		required: true
	},
	name:
	{
		type: String,
		required: true
	},

	ingredients:
	{
		type: String,
		required: true
	}

});

// Modèle de menu
const Plat = mongoose.model('Menu', platSchema);

module.exports = Plat;
