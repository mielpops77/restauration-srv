const express = require('express');
const router = express.Router();
const platController = require('../controllers/menuController');

// Route POST pour créer une nouvelle réservation
router.post('/', platController.createPlat);

router.get('/getmenu', platController.getMenu);

module.exports = router;
