
const Menu = require('../models/platModel');


exports.createPlat = async (req, res) => {
    try {
        // Récupérer les données de la réservation depuis le corps de la requête
        const { picture, price, type, name,ingredients } = req.body;
        // Créer une nouvelle instance du modèle menu
        const plat = new Menu({
            picture, price, type, name,ingredients
            // Autres champs de la réservation
        });


        // Enregistrer le menu dans la base de données
        await plat.save();


        res.status(201).json({ message: 'Plat créée avec succès' });
      
    } catch (error) {
        res.status(500).json({ error: 'Une erreur est survenue lors de la création du plat' });
    }
};





exports.getMenu = async (req, res) => {
    try {
        // Chercher la configuration de réservation dans la base de données
        const carte = await Menu.find();
        console.log(carte);

        if (!carte) {
            return res.status(404).json({ error: 'Menu non trouvée' });
        }

        res.status(200).json(carte);
    } catch (error) {
        res.status(500).json({ error: 'Une erreur est survenue lors de la récupération de la configuration de réservation' });
    }
};
